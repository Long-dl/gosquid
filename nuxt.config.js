import { resolve } from "path";
// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  alias: {
    '@': resolve(__dirname, "/")

  },
  css: ["~/assets/scss/main.scss"
  ],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  modules: [
    [
      '@nuxtjs/i18n',
      {
        lazy: true,
        langDir: "locales",
        strategy: "prefix_except_default",
        locales: [
          {
            code: "en-US",
            iso: "en-US",
            name: "English",
            file: "en-US.json"
          },
          {
            code: "vn-VNM",
            iso: "vn-VNM",
            name: "Vietnamese",
            file: "vn-VNM.json"
          },
        ],
        defaultLocale: "en-US",
        vueI18n: './i18n.config.js',
      }
    ]
  ],
})
