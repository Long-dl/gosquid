/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue",
  ],
  theme: {
    extend: {
      fontFamily: {
        'drone-ranger': ['Drone Ranger','sans-serif'],
        'inter': ['Inter', 'sans-serif']
      },
      width: {
        '21': '84px',
        '100': '31.6875rem',
        '120': '52.5rem'
      },
      colors: {
        'secondary':'#FBC625'
      },
      spacing: {
        '65':'17rem'
      },
      dropShadow:{
        'ms': '0px 4px 4px rgba(0, 0, 0, 0.25)'
      },
      transformOrigin: {
        'top-right-75': '75% top'
      },
      keyframes: {
        'growth': {
          '0%': { opacity: '0', transform: 'scale(0)'},
          '100%': { opacity: '1', transform: 'scale(1)'}
        }
      },
      animation: {
        growth: 'growth ease-in 0.25s'
      }
    },
  },
  plugins: [
    require('tailwindcss'),
    require('autoprefixer'),
],
}

